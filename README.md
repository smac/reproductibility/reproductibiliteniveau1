# ReproductibiliteNiveau1

Cette version correspond à la réexécution.
Le workflow est un peu mieux décrit (séparation génération/calculs, aide textuelle) et deux branches correspondent à des expérimentation que l'on peut ré-exécuter.
On n'obtient pas cependant le même résultat, le workflow est encore très faible et la séparation présentation/séparation données et code trop faible