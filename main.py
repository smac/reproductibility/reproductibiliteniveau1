import sys
import numberGenerator
import outputModule

if __name__ == '__main__':
    numbers = []
    if len(sys.argv) == 2 :
        numbers = numberGenerator.generateNumbers(int(sys.argv[1]))
        outputModule.computeValues(numbers)
    elif len(sys.argv) == 4 :
        numbers = numberGenerator.generateNumbers(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))
        outputModule.computeValues(numbers,int(sys.argv[2]),int(sys.argv[3]))
    else :
        print("Usage: python3 main.py [tries] [min] [max]")
        print("Example: python3 main.py 10 0 10")
        print("Example: python3 main.py 10")
        exit(1)