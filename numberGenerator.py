import random

def generateNumbers(tries,min=0,max=10) -> list[int]:
    numbers = []
    for _ in range (tries) :
        numbers.append(random.randint(min,max))
    print("Generated :" + str(numbers))
    return numbers