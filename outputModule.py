import matplotlib.pyplot as plt
import numpy as np

def computeValues(values,min=0,max=10) -> None:
    plt.hist(values,range=(min,max),bins=max+1,color='blue',edgecolor='black')
    print(f"average : {np.mean(values)}")
    plt.show()